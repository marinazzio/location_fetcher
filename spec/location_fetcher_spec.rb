# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LocationFetcher do
  subject { described_class }

  let(:query) { 'example.com' }

  it 'has a version number' do
    expect(LocationFetcher::VERSION).not_to be_nil
  end
end

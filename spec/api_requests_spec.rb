# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'successful query' do
  it 'gets successful response' do
    expect(subject.fetch(query).to_h).to include(successful_response)
  end
end

RSpec.shared_examples 'failure query' do
  it 'gets failure response' do
    expect(subject.fetch(query).to_h).to include(failure_response)
  end
end

RSpec.describe LocationFetcher do
  subject { described_class }

  let(:access_key) { SecureRandom.hex }
  let(:query) { '1.1.1.1' }

  let(:successful_response) do
    {
      status: :success,
      message: nil,
      ip: '1.1.1.1',
      latitude: 55.747928619384766,
      longitude: 37.723751068115234
    }
  end

  let(:failure_response) do
    {
      status: :failure,
      message: error_message
    }
  end

  let(:stubbed_url) { "#{described_class::Fetcher::API_URL}/#{query}?access_key=#{access_key}" }

  before do
    stub_request(:any, stubbed_url)
      .to_return_json(
        status: 200,
        body: response
      )
  end

  context 'with successful responses' do
    let(:response) { successful_response }

    before { ENV['LOCATION_FETCHER_IPSTACK_API_KEY'] = access_key }

    context 'with ip query' do
      it_behaves_like 'successful query'
    end

    context 'with url query' do
      let(:query) { 'example.com' }

      it_behaves_like 'successful query'
    end
  end

  context 'with error responses' do
    let(:response) { failure_response }

    context 'without access key' do
      let(:error_message) { 'No access key provided' }

      before { ENV.delete('LOCATION_FETCHER_IPSTACK_API_KEY') }

      it_behaves_like 'failure query'
    end

    context 'with wrong argument' do
      let(:error_message) { 'Wrong query argument' }

      let(:query) { '333' }

      before { ENV['LOCATION_FETCHER_IPSTACK_API_KEY'] = SecureRandom.hex }

      it_behaves_like 'failure query'
    end
  end
end

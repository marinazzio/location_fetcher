# frozen_string_literal: true

require 'active_support/core_ext/object/blank'

require_relative 'location_fetcher/version'
require_relative 'location_fetcher/api_response'
require_relative 'location_fetcher/fetcher'

module LocationFetcher
  class NoApiKeyError < StandardError
    def message = 'No access key provided'
  end

  class IncorrectInputError < StandardError
    def message = 'Wrong query argument'
  end

  class ApiConnectionError < StandardError; end
  class ApiClientError < StandardError; end

  def self.fetch(query)
    Fetcher.new(query).fetch
  end
end

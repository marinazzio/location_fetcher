module LocationFetcher
  class ApiResponse
    def self.success(message: nil, payload: '{}')
      new(
        status: :success,
        message:,
        payload:
      )
    end

    def self.failure(message:, payload: '{}')
      new(
        status: :failure,
        message:,
        payload:
      )
    end

    attr_reader :status, :message, :payload

    def initialize(status:, message:, payload:)
      @status = status
      @message = message
      @payload = JSON.parse(payload)
    end

    def to_h
      {
        status:,
        message:,
        payload:
      }.tap do |resp|
        return resp if failure?

        resp[:ip] = payload['ip']
        resp[:latitude] = payload['latitude']
        resp[:longitude] = payload['longitude']
      end
    end

    private

    def failure?
      status == :failure
    end
  end
end

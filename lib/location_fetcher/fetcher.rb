# frozen_string_literal: true

require 'net/http'

module LocationFetcher
  class Fetcher
    API_URL = 'http://api.ipstack.com'
    DOMAIN_NAME_REGEX = /^(?!:\/\/)(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$/i

    attr_reader :api_key, :query, :http

    def initialize(query)
      @api_key = ENV.fetch('LOCATION_FETCHER_IPSTACK_API_KEY', nil)
      @query = query

      uri = URI(API_URL)
      @http = Net::HTTP.new(uri.host, uri.port)
    end

    def fetch
      validate_api_key
      validate_query

      perform_request
    rescue IncorrectInputError, NoApiKeyError => e
      ApiResponse.failure(message: e.message)
    end

    private

    def perform_request
      response = http.request_get(request_string)

      raise ApiConnectionError, 'Connection error' if response.code_type != Net::HTTPOK
      raise ApiClientError, response['error']['info'] if successful_response?(response)

      ApiResponse.success(payload: response.body)
    rescue ApiConnectionError, ApiClientError => e
      ApiResponse.failure(message: e.message, payload: response.body)
    end

    def successful_response?(response)
      response['success'].present? && response['success']
    end

    def request_string
      "/#{query}?access_key=#{api_key}"
    end

    def validate_api_key
      raise NoApiKeyError if api_key.blank?
    end

    def validate_query
      raise IncorrectInputError if query.blank? || (!ip?(query) && !domain_name?(query))
    end

    def ip?(query)
      !!IPAddr.new(query)
    rescue # rubocop: disable Style/RescueStandardError
      false
    end

    def domain_name?(query)
      !!(query =~ DOMAIN_NAME_REGEX)
    end
  end
end

# frozen_string_literal: true

require_relative 'lib/location_fetcher/version'

Gem::Specification.new do |spec|
  spec.name = 'location_fetcher'
  spec.version = LocationFetcher::VERSION
  spec.authors = ['Denis Kiselev']
  spec.email = ['denis.kiselyov@gmail.com']

  spec.summary = 'Fetch geolocation by IP or URL'
  spec.description = 'Fetch geolocation data using ipstack.com service.'
  spec.required_ruby_version = '>= 3.2.0'

  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor Gemfile])
    end
  end

  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '~> 7.1.2'
end
